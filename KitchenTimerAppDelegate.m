//
//  KitchenTimerAppDelegate.m
//  KitchenTimer
//
//  Created by Lewis Garrett on 3/7/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import "KitchenTimerAppDelegate.h"
#import "KitchenTimerDocumentController.h"

@implementation KitchenTimerAppDelegate

@synthesize window;

- (void)applicationWillFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application 
	KitchenTimerDocumentController *kitchenTimerDocumentController = [[KitchenTimerDocumentController alloc] init];
	[kitchenTimerDocumentController release];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application 
}

@end
