//
//  NamedTimerSettings.h
//  KitchenTimer
//
//  Created by Lewis Garrett on 2/22/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TimerDocument.h"


@interface NamedTimerSettings : NSWindowController {

	TimerDocument *timerDocument;

	IBOutlet NSTextField *timerNameTextField;
	IBOutlet NSPopUpButton *timerSoundPopupButton;
}
@property (retain) IBOutlet NSTextField *timerNameTextField;
@property (retain) IBOutlet NSPopUpButton *timerSoundPopupButton;

-(IBAction)createTimerButtonAction:(id)sender;
-(IBAction)cancelButtonAction:(id)sender;
-(IBAction)timerNameTextFieldAction:(id)sender;

@end
