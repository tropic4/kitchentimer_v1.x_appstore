//
//  KitchenTimerAppDelegate.h
//  KitchenTimer
//
//  Created by Lewis Garrett on 3/7/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KitchenTimerAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
