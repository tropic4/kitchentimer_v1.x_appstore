//
//  main.m
//  KitchenTimer
//
//  Created by Lewis Garrett on Fri May 30 2003.
//  Copyright (c) 2003 Iota. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// Global variables
int gTimerNumber = 1;

int main(int argc, const char *argv[])
{
    return NSApplicationMain(argc, argv);
}
