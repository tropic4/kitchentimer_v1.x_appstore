//
//  KitchenTimerDocumentController.h
//  KitchenTimer
//
//  Created by Lewis Garrett on 3/8/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class NamedTimerSettings;

@interface KitchenTimerDocumentController : NSDocumentController {

    NamedTimerSettings *namedTimerSettings;					
}

@end
