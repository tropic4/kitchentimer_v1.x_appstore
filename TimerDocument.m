//
//  TimerDocument.m
//  KitchenTimer
//
//  Created by Lewis Garrett on Fri May 30 2003.
//  Copyright (c) 2003 Iota. All rights reserved.
//

#import "TimerDocument.h"
#import "NamedTimerSettings.h"

#import <mach/mach_port.h>
#import <mach/mach_interface.h>
#import <mach/mach_init.h>

#import <IOKit/pwr_mgt/IOPMLib.h>
#import <IOKit/IOMessage.h>

// Global variables
extern int gTimerNumber;

io_connect_t	root_port;
int				allowPowerChange = 0;
//NSTimeInterval  intervalOfSystemSleep = 0;							//leg20110227 - experimental code
//BOOL			systemDidSleep = NO;									//leg20110227 - experimental code
// Experiment trying to make timer adjustment for System Sleep.			//leg20110227 - experimental code
//	Found that there is always a time lag until the App is notified		//leg20110227 - experimental code
//	that the system is awake with the result that the adjustment is
//	always off by 5-10 seconds.  Also, I realized at the end that the
//	adjustment code only works for the first timer because it depends
//	on a global value.  To work properly would require maintaining a
//	separate adjustment counter for each timer.
// If this code is resurrected, consider using new Leopard APIs
//	detailed in Q&A QA1340

// Power Management Services callback routine
void callback(void * x,io_service_t y,natural_t messageType,void * messageArgument)
{
    //printf("KitchenTimer messageType %08lx, arg %08lx\n",(long unsigned int)messageType,
    //        (long unsigned int)messageArgument);
    switch ( messageType ) {
    case kIOMessageSystemWillSleep:
		{
/*
			// Save the start of System Sleep						//leg20110227 - experimental code
			IOAllowPowerChange(root_port,(long)messageArgument);
			NSDate*	dateOfSystemSleep = [NSDate date];
			intervalOfSystemSleep = [dateOfSystemSleep timeIntervalSinceReferenceDate];
*/
			printf("KitchenTimer kIOMessageSystemWillSleep\n");
		}
        break;
    case kIOMessageCanSystemSleep:
        if (allowPowerChange) {
            IOCancelPowerChange(root_port,(long)messageArgument);
			
            printf("KitchenTimer IOCancelPowerChange.\n");
        }
        else {
            IOAllowPowerChange(root_port,(long)messageArgument);
			
            printf("KitchenTimer IOAllowPowerChange.\n");
        }
        break;
    case kIOMessageSystemHasPoweredOn:
		{
/*
			// Determine how long we were asleep					//leg20110227 - experimental code
			NSDate*	dateOfSystemWake = [NSDate date];
			NSTimeInterval  intervalSinceSystemSleep = 0;
			intervalSinceSystemSleep = [dateOfSystemWake timeIntervalSinceReferenceDate];
			intervalOfSystemSleep = intervalSinceSystemSleep - intervalOfSystemSleep;
			systemDidSleep = YES;
*/			
			printf("KitchenTimer kIOMessageSystemHasPoweredOn\n");
		}
        break;
    }    
}

@implementation TimerDocument

/*
- (BOOL)autoenablesItems
{
	return YES;
}
*/

- (id)init
{
    self = [super init];
    if (self) {
    
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
        
        // initialize timer values
        secondsTime = 0;
        minutesTime = 0;
        hoursTime = 0;
        lastCountDownTime = 0;
		
		// Establish defaults for named timers		//leg20110222 - 1.0.1
		timerName = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"TimerName"];
		timerSound = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"SoundName"];
		isNamedTimer = [[NSUserDefaults standardUserDefaults] boolForKey:@"IsNamedTimer"];
/*		
		timerName = @"Timer";
		[[NSUserDefaults standardUserDefaults]
			setObject:timerName forKey:@"TimerName"];
			
		timerSound = @"Glass";
		[[NSUserDefaults standardUserDefaults]
			setObject:timerSound forKey:@"SoundName"];
			
		BOOL isNamedTimer = NO;
		[[NSUserDefaults standardUserDefaults]
			setBool:isNamedTimer forKey:@"IsNamedTimer"];
*/
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil timerName:(NSString*)name timerSound:(NSString*)sound {	//leg20110222 - 1.0.1
	
    //if (self = [super initWithNibName:nibNameOrNil bundle:nil]) {
    if (self = [super init]) {
	}
	[self windowControllerDidLoadNib: nil];
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"TimerWindow";
}

- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
	[super windowControllerDidLoadNib:aController];
	// Add any code here that needs to be executed once the windowController has loaded the document's window.

	// Make the window controller object handy for later.
	myWindowController = aController;

	// See if this timer document will have a name and sound provided by the user
	if (!isNamedTimer) {
		// This is not a named timer

		// Set the name of the sound to use on timer expiration
        timerSound = (NSMutableString*)@"Glass";

		//NSString *timerName = [NSString stringWithFormat:@"Timer %d", gTimerNumber++];
		NSString *timerNameString = [NSString stringWithFormat:@"Timer %d", gTimerNumber++];
		NSString *token;
		NSString *documentName;

		// Get the name of the file ("Untitled").  Could not use "- (NSString *)fileName;"
		//  because that method returns nil if file is untitled.
		documentName = [self displayName];

		// Break displayName into tokens in case this is not the first instance of TimerDocument.   
		NSArray *listItems = [documentName componentsSeparatedByString:@" "];
		
		// Isolate "Untitled" from displayName
		token = [listItems objectAtIndex:0];

		// Put displayName in an NSMutableString so that we can alter it.    
		windowTitle = [NSMutableString stringWithString:documentName];

		// Change "Untitled" to "Timer" leaving the number suffix (if any).    
		[windowTitle replaceOccurrencesOfString:token
												withString:timerNameString
												options:NSLiteralSearch
												range:NSMakeRange(0, [windowTitle length])];
	} else {
		// This is a named timer								//leg20110222 - 1.0.1

		// Get the name and sound for the timer		
		timerName = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"TimerName"];
		if (timerName == nil || [@"" isEqual: timerName])
			timerName = (NSMutableString*)@" ";

		// Name the timer window.
		windowTitle = timerName;
			
		timerSound = (NSMutableString*)[[NSUserDefaults standardUserDefaults] stringForKey:@"SoundName"];
		if (timerSound == nil || [@"" isEqual: timerSound])
			timerSound = (NSMutableString*)@"Glass";

		// Make sure named timer flag is set off.
		[[NSUserDefaults standardUserDefaults]
			setBool:NO forKey:@"IsNamedTimer"];
		[[NSUserDefaults standardUserDefaults] synchronize];	//leg20110309 - 1.0.1

	}
	
	// Put the first timer at the upper left of the screen		//leg20110309 - 1.0.1
	if (gTimerNumber == 2) {
		NSPoint point;
		point.x = 30.0;
		point.y = 30.0;
		point = [[myWindowController window] cascadeTopLeftFromPoint: point];
	}

	// Set fileName to the "changed" name.  This will cause the document
	//  name (and the window title) to be synchronized with the fileName.  
	//[self setFileName:windowTitle];
	// -setFileName deprecated - use -setFileURL							
	NSString *urlString = [NSString stringWithFormat:@"file:///%@", windowTitle];
	NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	[self setFileURL:url];

    // Slow down the typomatic (continuous) effect of the hours,
    //  minutes, and seconds buttons
    float delay = 0.4;
    float interval = 0.150;
    [hoursButton setPeriodicDelay:delay interval:interval];
    [minutesButton setPeriodicDelay:delay interval:interval];
    [secondsButton setPeriodicDelay:delay interval:interval];
    
    // After the window appears, update the display
    [self updateTimerDisplay];
}

- (NSData *)dataRepresentationOfType:(NSString *)aType
{
    // Insert code here to write your document from the given data.  You can also choose to override -fileWrapperRepresentationOfType: or -writeToFile:ofType: instead.
    return nil;
}

- (BOOL)loadDataRepresentation:(NSData *)data ofType:(NSString *)aType
{
    // Insert code here to read your document from the given data.  You can also choose to override -loadFileWrapperRepresentation:ofType: or -readFromFile:ofType: instead.
    return YES;
}

- (void)windowWillClose:(NSNotification *)notification
{
    if (timerIsRunning) {
        // Invalidate and release the timer
        [timer invalidate];
        [timer release];
    }
}

- (void)dealloc
{
	[namedTimerSettings release];				//leg20110222 - 1.0.1
	
    [super dealloc];
}

//
//� Action methods
//
- (IBAction)incrSeconds:(id)sender
{
    secondsTime++;
    if (secondsTime > 59)
        secondsTime = 0;
        
    [self updateTimerDisplay];
}

- (IBAction)incrMinutes:(id)sender
{
    minutesTime++;
    if (minutesTime > 59)
        minutesTime = 0;
        
    [self updateTimerDisplay];
}

- (IBAction)incrHours:(id)sender
{
    hoursTime++;
    if (hoursTime > 23)
        hoursTime = 0;
        
    [self updateTimerDisplay];
}

- (IBAction)startStopTimer:(id)sender
{
    IONotificationPortRef	notify;
    io_object_t 		anIterator;

    // First press of start/stop button:  start timer.
    if ([sender state] == 1) {
        timerJustCountedDown = NO;
        timerIsRunning = YES;
        lastCountDownTime = (hoursTime*3600)+(minutesTime*60)+secondsTime;
        
        // Clear status display.
        [statusDisplay setObjectValue:@" "];

        // If timer setting is zero then we are counting up instead of down.
        if (lastCountDownTime == 0)
            timerIsCountingUp = YES;
        
        // Create a timer
        timer = [[NSTimer scheduledTimerWithTimeInterval:1.0
                                        target:self
                                        selector:@selector(seeIfTimeIsUp:)
                                        userInfo:nil
                                        repeats:YES] retain];

        // Register to receive Power Manager notifications
        fprintf(stderr, "\nKitchenTimer attempting to register for system power notifications\n");
        root_port = IORegisterForSystemPower (0,&notify,callback,&anIterator);
        if ( root_port == 0 ) {
            fprintf(stderr, "KitchenTimer IORegisterForSystemPower failed\n");
        }
        else
            fprintf(stderr, "KitchenTimer will prevent any System Power change while a timer is running.\n");
    
		CFRunLoopAddSource(CFRunLoopGetCurrent(),
                            IONotificationPortGetRunLoopSource(notify),
                            kCFRunLoopDefaultMode);
        allowPowerChange++;
                        
        // Disable all buttons except start/stop while timer is running.
        [resetButton setEnabled:NO];
        [hoursButton setEnabled:NO];
        [minutesButton setEnabled:NO];
        [secondsButton setEnabled:NO];

    // Second press of start/stop button:  stop timer.
    } else {
        timerJustCountedDown = YES;
        timerIsRunning = NO;
        timerHasExpired = NO;
        timerIsCountingUp = NO;
        
        // Re-enable all buttons except start/stop when timer has been stoped.
        [resetButton setEnabled:YES];
        [hoursButton setEnabled:YES];
        [minutesButton setEnabled:YES];
        [secondsButton setEnabled:YES];

        // Now that timer has expired, deregister receiving Power Manager notifications
        IOReturn result =
            IODeregisterForSystemPower (&anIterator);
        fprintf(stderr, "KitchenTimer IODeregisterForSystemPower, IOReturn=%08lx.\n",
                            (long unsigned int)result);

        // PM DDK for Mac OS X 10.1.x & 10.2 - Chapter 10 Power Management, page 149
        //  of IOKit Fundamentals says to IODeregisterForSystemPower when Power Manager
        //  notifications are no longer needed.  It doesn't work.  As long as application
        //  is still running, the callback function continues to receive the notifications. 
        //  Therefore, it was necessary to put a conditional in the callback routine to
        //  decide when it is necessary to suppress or allow power changes.  The global
        //  int allowPowerChange is used for that purpose.  As long as allowPowerChange
        //  is positive, the power change will be suppressed.
        allowPowerChange--;

        // Invalidate and release the timer
        [timer invalidate];
        [timer release];
    }
    
    [self updateTimerDisplay];
}

-(IBAction)resetTimer:(id)sender
{
    int temporary;
    
    // If the timer just counted down then set the timer display to the
    //  values used for the last count down, else zero the timer values.
    if (timerJustCountedDown) {        
        // Turn count down flag off
        timerJustCountedDown = NO;

        // Break lastCountDownTime into hours, minutes, and seconds.
        hoursTime = lastCountDownTime/3600;
        temporary = (lastCountDownTime - (hoursTime*3600));
        minutesTime = temporary/60;
        secondsTime = (temporary - (minutesTime*60));
    } else {        
        secondsTime = 0;
        minutesTime = 0;
        hoursTime = 0;
    }
    
    [self updateTimerDisplay];
}

// Go to Tropical Software's web site.
- (IBAction)openWebSiteInDefaultBrowser:(id)sender
{
  [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.tropic4.com"]];
}


// Throw up window to set the name of the Timer				//leg20110222 - 1.0.1
-(IBAction)showNamedTimerSettingsWindow:(id)sender		
{
	// Lazy instantiation of controller
	if (!namedTimerSettings) {
		namedTimerSettings = [[NamedTimerSettings alloc] initWithWindowNibName:@"NamedTimerSettings"];
	}
	[namedTimerSettings showWindow:sender];
	[[namedTimerSettings window] center];
}

// Set the timer display.
- (void)updateTimerDisplay
{
    NSCalendarDate *yearZero;

/*    
	// If a System Sleep has intervened, adjust the timer appropriately			//leg20110227 - experimental code
	if (systemDidSleep) {	
		// Break lastCountDownTime into hours, minutes, and seconds.
		int temporary, temporaryInterval=0;
		
		temporaryInterval += secondsTime;
		temporaryInterval += minutesTime*60;
		temporaryInterval += hoursTime*3600;
		
		if (!timerIsCountingUp) 
			temporaryInterval -= intervalOfSystemSleep;
		else
			temporaryInterval += intervalOfSystemSleep;	
		
		if (temporaryInterval < 0) {
			timerHasExpired = YES;
			temporaryInterval =abs(temporaryInterval);
		}
		
		hoursTime = temporaryInterval/3600;
		temporary = (temporaryInterval - (hoursTime*3600));
		minutesTime = temporary/60;
		secondsTime = (temporary - (minutesTime*60));
		
		systemDidSleep = NO;
		intervalOfSystemSleep = 0;
	}
*/
	
    // Create a date representing the year 0 Common Era.
    yearZero = [NSCalendarDate calendarDate];
    [yearZero initWithYear:0 month:0 day:0 hour:0 minute:0 second:0 timeZone:0];
    
    // Add our timer value to the zero year so that date will represent only
    // 	the hours, minutes and seconds of our timer.
    NSCalendarDate *timerValue = [yearZero dateByAddingYears:0 
                                    months:0
                                    days:0
                                    hours:hoursTime
                                    minutes:minutesTime
                                    seconds:secondsTime];

    // NSDateFormatter will display timer value in the form:  HH:MM:SS.
    [timerDisplay setObjectValue:timerValue];
    
    // Update status display.
    if (timerHasExpired)
        [statusDisplay setObjectValue:@"** TIME IS UP **"];
    else if (timerIsRunning) 
        if (timerIsCountingUp)
            [statusDisplay setObjectValue:@"** COUNT-UP **"];
        else
            [statusDisplay setObjectValue:@"** COUNT-DOWN **"];
    else
        [statusDisplay setObjectValue:@" "];
}

//
//� Private methods
//
- (void)seeIfTimeIsUp:(NSTimer *)aTimer
{
    int secondsRemaining, temporary;
    
    // Calculate the number of seconds left before timer expires.
    secondsRemaining = (hoursTime*3600)+(minutesTime*60)+secondsTime;
    
    // If timer has expired start counting up, else count down.
    if (timerHasExpired || timerIsCountingUp)
        secondsRemaining++;
    else
        secondsRemaining--;
    
    // Break secondsRemaining into hours, minutes, and seconds.
    hoursTime = secondsRemaining/3600;
    temporary = (secondsRemaining - (hoursTime*3600));
    minutesTime = temporary/60;
    secondsTime = (temporary - (minutesTime*60));
    
    // If seconds <= zero, flag the timer as having expired and load up the alarm sound.
    if ((secondsRemaining <= 0) && !timerIsCountingUp) {
        timerHasExpired = YES;
        //alarmSound = [NSSound soundNamed:@"Glass"];
        alarmSound = [NSSound soundNamed:timerSound];					//leg20110222 - 1.0.1

        // De-miniaturize window (if it is miniaturized).
        [[myWindowController window] deminiaturize: nil];
    }

    // If timer has expired, signal to user by beeping every second.
    if (timerHasExpired) {
        //NSBeep();
        //NSSound *sound = [NSSound soundNamed:@"Glass"];
        //NSSound *airplaneSound = [NSSound soundNamed:@"Airplane_44KStereo"];

        [alarmSound play];
        //[sound pause];
        //[sound resume];
        //[sound stop];
     }
    
    [self updateTimerDisplay];
}

#pragma mark Menu Management
//leg20110307 - 1.0.
- (BOOL)validateMenuItem:(NSMenuItem *)item {
    if ([item action] == @selector(showNamedTimerSettingsWindow:)) {
		[item setEnabled: YES];
        return YES;
    }
    if ([item action] == @selector(newDocument)) {
        return YES;
    }
    return YES;
}

@end
