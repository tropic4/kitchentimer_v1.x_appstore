//
//  KitchenTimerDocumentController.m
//  KitchenTimer
//
//  Created by Lewis Garrett on 3/8/11.
//  Copyright 2011 Iota. All rights reserved.
//

#import "KitchenTimerDocumentController.h"
#import "NamedTimerSettings.h"

@implementation KitchenTimerDocumentController

// Throw up window to set the name of the Timer				
-(IBAction)showNamedTimerSettingsWindow:(id)sender		
{
	// Lazy instantiation of controller
	if (!namedTimerSettings) {
		namedTimerSettings = [[NamedTimerSettings alloc] initWithWindowNibName:@"NamedTimerSettings"];
	}
	[namedTimerSettings showWindow:sender];
	[[namedTimerSettings window] center];
}

- (IBAction)newDocument:(id)sender
{
	int tag = [sender tag];
	if (tag==47) {
		[self showNamedTimerSettingsWindow: sender];
	}
	else {
		[super newDocument:sender];
	}
}

/*
- (id)openUntitledDocumentAndDisplay:(BOOL)displayDocument error:(NSError **)outError
{
	id doc = [super openUntitledDocumentAndDisplay: YES error: outError];
	return doc;
}
*/

@end
